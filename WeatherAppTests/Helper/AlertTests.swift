//
//  AlertTests.swift
//  WeatherApp
//
//  Created by Ashish Patel on 2019/02/12.
//  Copyright © 2019. All rights reserved.
//

import XCTest
@testable import WeatherApp

class AlertTests: XCTestCase {
    
    func testAlert() {
        let expectAlertActionHandlerCall = expectation(description: "Alert action handler called")
        let alert = SingleButtonAlert(
            title: "",
            message: "",
            action: AlertAction(buttonTitle: "", handler: {
                expectAlertActionHandlerCall.fulfill()
            })
        )
        alert.action.handler!()
        waitForExpectations(timeout: 0.1, handler: nil)
    }
}
