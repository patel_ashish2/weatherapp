//
//  Weather.swift
//  WeatherApp
//
//  Created by Ashish Patel on 2019/02/09.
//  Copyright © 2019. All rights reserved.
//

import Foundation

//
//struct Forecast {
//    
//    let date: NSDate
//    let temp: Float
//    let description: String
//}
//
//extension Forecast{
//    
//    init?(json: JSON) {
//        guard
//            let timestamp = json["dt"].double,
//            let temp = json["main"]["temp_max"].float,
//            let description = json["weather"][0]["main"].string
//            else {
//                return nil
//        }
//        
//        self.date = NSDate(timeIntervalSince1970: timestamp)
//        self.temp = temp
//        self.description = description
//    }
//}
//
//
//struct Weather {
//    
//    let cityName: String
//    let forecasts: [Forecast]
//    
//}
//extension Weather{
//    
//    init?(json: JSON) {
//        
//        guard  let cityName = json["city"]["name"].string,
//            let forecastData = json["list"].array
//            else {
//                return nil
//        }
//        
//        self.cityName = cityName
//        let forecasts = forecastData.flatMap(Forecast.init)
//        guard !forecasts.isEmpty else {
//            return nil
//        }
//        
//        self.forecasts = forecasts
//    }
//}

struct WeeklyWeather : Codable {
    let cod : String?
    let message : Double?
    let cnt : Int?
    let list : [Forecast]?
    let city : City?
    
    enum CodingKeys: String, CodingKey {
        
        case cod = "cod"
        case message = "message"
        case cnt = "cnt"
        case list = "list"
        case city = "city"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cod = try values.decodeIfPresent(String.self, forKey: .cod)
        message = try values.decodeIfPresent(Double.self, forKey: .message)
        cnt = try values.decodeIfPresent(Int.self, forKey: .cnt)
        list = try values.decodeIfPresent([Forecast].self, forKey: .list)
        city = try values.decodeIfPresent(City.self, forKey: .city)
    }
    
}


