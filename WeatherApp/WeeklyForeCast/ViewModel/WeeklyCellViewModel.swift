//
//  WeeklyCellViewModel.swift
//  WeatherApp
//
//  Created by Ashish Patel on 2019/02/09.
//  Copyright © 2019. All rights reserved.
//

import Foundation



protocol WeeklyCellViewModel {
    
    //var forCast: Forecast { get }
    var tempStr:String{ get }
    var dayStr:String { get }
    //var weeklyWeather: Weather { get }
}

extension Forecast: WeeklyCellViewModel{
    
    var tempStr: String {
            return String(format: "%.2f", self.main?.temp ?? "")
    }
    var dayStr : String{
        if let dayString = self.weather?.first!.main {
            return dayString
        }
        return ""
    }
}
