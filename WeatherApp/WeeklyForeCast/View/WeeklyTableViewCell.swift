//
//  WeeklyTableViewCell.swift
//  WeatherApp
//
//  Created by Ashish Patel on 2019/02/09.
//  Copyright © 2019. All rights reserved.
//

import UIKit

class WeeklyTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var imgWeatherIcon: UIImageView!
    @IBOutlet weak var lblTemperature: UILabel!
    
    func configureCell(_ day:String?, icon:UIImage?, temp:String?){
        self.lblDay.text = day
        self.imgWeatherIcon.image = icon
        self.lblTemperature.text = temp
    }
    
    var viewModel: WeeklyCellViewModel? {
        didSet {
            bindViewModel()
        }
    }

    private func bindViewModel() {
        if let temp = viewModel?.tempStr{
            lblTemperature.text = temp
        }
        if let day = viewModel?.dayStr {
            print(day)
           lblDay.text = day
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
