//
//  Result.swift
//  WeatherApp
//
//  Created by Ashish Patel on 2019/02/08.
//  Copyright © 2019. All rights reserved.
//

import Foundation



enum Result<T, U> where U: Error {
    case success(payload: T)
    case failure(U?)
}

enum EmptyResult<U> where U: Error {
    case success
    case failure(U?)
}
