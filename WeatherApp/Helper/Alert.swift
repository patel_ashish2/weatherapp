//
//  Alert.swift
//  WeatherApp
//
//  Created by Ashish Patel on 2019/02/08.
//  Copyright © 2019. All rights reserved.
//

import Foundation

struct AlertAction {
    let buttonTitle: String
    let handler: (() -> Void)?
}

struct SingleButtonAlert {
    let title: String
    let message: String?
    let action: AlertAction
}
