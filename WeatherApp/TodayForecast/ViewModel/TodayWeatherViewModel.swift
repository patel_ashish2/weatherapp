//
//  TodayWeatherViewModel.swift
//  WeatherApp
//
//  Created by Ashish Patel on 2019/02/09.
//  Copyright © 2019. All rights reserved.
//

import Foundation

class TodayWeatherViewModel  {

    let appServerClient: AppServerClient
    var onShowError: ((_ alert: SingleButtonAlert) -> Void)?
    let showLoadingHud: Bindable = Bindable(false)
    let description = Bindable(String.init())
    var minTemp = Bindable(String.init())
    var currentTemp = Bindable(String.init())
    var maxTemp = Bindable(String.init())
    
    init(appServerClient: AppServerClient = AppServerClient()) {
        self.appServerClient = appServerClient
    }
    // current weather of city with lat anad lon
    func getCurrentWeatherData(params: [(String, String)]){
        showLoadingHud.value = true
        appServerClient.getTodayWeather(params:params){ [weak self] result in
            guard let strongSelf = self  else { return }
            strongSelf.showLoadingHud.value = false
            switch result {
            case .success(let data):
                if let description = data.weather?.first?.main {
                    strongSelf.description.value = description
                }
                if let temp = data.main?.temp {
                    strongSelf.currentTemp.value = String(Int(round(temp-273.15))) + "° Current"
                }
                if let temp_min = data.main?.temp_min {
                    strongSelf.minTemp.value = String(Int(round(temp_min-273.15))) + "°   Min"
                }
                if let temp_max = data.main?.temp_max {
                    strongSelf.maxTemp.value = String(Int(round(temp_max-273.15))) + "°   Max"
                }

            case .failure(let error):
                let okAlert = SingleButtonAlert(
                    title: error?.getErrorMessage() ?? "Could not connect to server. Check your network and try again later.",
                    message: "Failed to update information.",
                    action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") })
                )
                self?.onShowError?(okAlert)
            }
        }
        
    }
}

// failur message
fileprivate extension AppServerClient.TodayDataFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .invalidkey:
            return "Your Key is Invalid"
        case .notFound:
            return "Api Key is missing "
        }
    }
}
